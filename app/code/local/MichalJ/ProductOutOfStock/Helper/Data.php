<?php
/**
 * Created by Michal Jozwiak
 * Project: MichalJ_ProductOutOfStock
 * Email: <michu.jozwiak@gmail.com>
 */

class MichalJ_ProductOutOfStock_Helper_Data
    extends Mage_Core_Helper_Abstract
{
    /**
     * @return mixed
     */
    public function getEnabled()
    {
        return Mage::getStoreConfig('productOutOfStock/options/enabled');
    }
}