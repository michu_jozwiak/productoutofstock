/**
 * Created by Michal Jozwiak
 * Project: MichalJ_ProductOutOfStock
 * Email: <michu.jozwiak@gmail.com>
 */
Product.Config.prototype.fillSelect = function(element) {
    var attributeId = element.id.replace(/[a-z]*/, '');
    var options = this.getAttributeOptions(attributeId);
    this.clearSelect(element);
    element.options[0] = new Option('', '');
    element.options[0].innerHTML = this.config.chooseText;

    var prevConfig = false;
    if(element.prevSetting){
        prevConfig = element.prevSetting.options[element.prevSetting.selectedIndex];
    }

    if(options) {
        var index = 1;
        for(var i=0;i<options.length;i++){
            var allowedProducts = [];

            if(prevConfig) {
                for(var j=0;j<options[i].products.length;j++){
                    if(prevConfig.config.allowedProducts
                        && prevConfig.config.allowedProducts.indexOf(options[i].products[j])>-1){
                        allowedProducts.push(options[i].products[j]);
                    }
                }
            } else {
                allowedProducts = options[i].products.clone();
            }


            if(allowedProducts.size()>0){
                options[i].allowedProducts = allowedProducts;
                element.options[index] = new Option(this.getOptionLabel(options[i], options[i].price), options[i].id);
                if (typeof options[i].price != 'undefined') {
                    element.options[index].setAttribute('price', options[i].price);
                }

                // DEV NOTE: add disabled attribute to option and change label
                var stockElements = []
                if (options[i]) {
                    for(var z=0; z<=options[i].allowedProducts.length;z++){
                        if (prevConfig) {
                            var isInStock = options[i].isInStock;

                            for (var x = 0; x<prevConfig.config.allowedProducts.length; x++) {
                                if (isInStock[prevConfig.config.allowedProducts[x]]) {
                                    stockElements.push(isInStock[prevConfig.config.allowedProducts[x]]);
                                }
                            }

                            for (var x = 0; x < stockElements.length - 1; x++) {
                                if (options[i].label == stockElements[x][1] && options[i].stockText == stockElements[x][0]) {
                                    element.options[index].setAttribute('disabled', true);

                                    var currentText = element.options[index].text;
                                    element.options[index].setAttribute('label', currentText + ' - ' + options[i].stockText);
                                }
                            }

                        } else {
                            var productOnStock = true;
                            for (var x = 0; x<options[i].allowedProducts.length; x++) {
                                stockElements.push(options[i].isInStock[options[i].allowedProducts[x]]);
                            }

                            for(var x = 0; x < stockElements.length - 1; x++ ){
                                if (stockElements[x][0] != stockElements[x+1][0]) {
                                    productOnStock = false;
                                }
                            }
                            if (productOnStock && stockElements[0][0] == options[i].stockText){
                                if (options[i].isInStock[options[i].allowedProducts[z]]){

                                    element.options[index].setAttribute('disabled', true);

                                    var currentText = element.options[index].text;
                                    element.options[index].setAttribute('label', currentText + ' - ' + options[i].stockText);
                                }
                            }
                        }

                    }

                }
                //DEV NOTE: END

                element.options[index].config = options[i];
                index++;
            }
        }
    }
};

Product.Config.prototype.fillSelect.allValuesSame = function(isInStock) {

};